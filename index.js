let request = require('request');
let yargs   = require('yargs');

const argv = yargs
    .option(
        'a', {
            alias: 'address',
            demand: true,
            describe: 'Address to fetch the weather forecast',
            string: true,
        })
    .help()
    .alias('h', 'help')
    .argv;

const StateEnum = {
  GET_LATITUDE_LONGITUDE: 0,
  GET_ONEMAP_TOKEN      : 1,
  GET_TOWN_NAME         : 2,
  GET_NEA_FORECAST      : 3
}

let currentState = StateEnum.GET_LATITUDE_LONGITUDE;

/* The main handler for all of the async callbacks
 *
 * The args is objects to process
 */
function stateHandler(args) {
  switch(currentState) {
    case StateEnum.GET_LATITUDE_LONGITUDE: {
      request({
        url: 'https://developers.onemap.sg/commonapi/search',
        qs : {
          searchVal      : argv.a,
          returnGeom     : 'Y',
          getAddrDetails : 'N',
        },
      }, (error, response, body) => {
        let bodyObj = JSON.parse(body);
        currentState = StateEnum.GET_ONEMAP_TOKEN;

        /* Object to process in the next stage */
        retObj = {
          longitude: bodyObj.results[0].LONGITUDE,
          latitude:  bodyObj.results[0].LATITUDE
        };

        /* Put this stateHandler into the system queue for the next step */
        setTimeout(stateHandler, 0, retObj);
      });
      break;
    }

    case StateEnum.GET_ONEMAP_TOKEN: {
      //console.log('Longitude:', args.longitude);
      //console.log('Latitude:', args.latitude);
      request({
        method: 'POST',
        url   : 'https://developers.onemap.sg/privateapi/auth/post/getToken',
        headers: { 
          'cache-control': 'no-cache',
          'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' 
        },
        formData: {
          email    : 'put_your_email_here',
          password : 'put_your_password_here',
        },
      }, (error, response, body) => {
        let bodyObj = JSON.parse(body);
        currentState = StateEnum.GET_TOWN_NAME;

        /* Object to process in the next stage */
        retObj = {
          longitude: args.longitude,
          latitude : args.latitude,
          token    : bodyObj.access_token,
        };

        /* Put this stateHandler into the system queue for the next step */
        setTimeout(stateHandler, 0, retObj);
      });
      break;
    }

    case StateEnum.GET_TOWN_NAME: {
      //console.log('Longitude:', args.longitude);
      //console.log('Latitude:', args.latitude);
      //console.log('Token:', args.token);
      request({
        url: 'https://developers.onemap.sg/privateapi/popapi/getPlanningarea',
        qs: {
          token: args.token,
          lat  : args.latitude,
          lng  : args.longitude,
        }
      }, (error, response, body) => {
        let bodyObj = JSON.parse(body);
        currentState = StateEnum.GET_NEA_FORECAST;

        /* Object to process in the next stage */
        retObj = {
          town: bodyObj[0].pln_area_n
        };

        /* Put this stateHandler into the system queue for the next step */
        setTimeout(stateHandler, 0, retObj);
      });
      break;
    }

    case StateEnum.GET_NEA_FORECAST: {
      let currentDateTime = new Date();
      let forecastDateTime = currentDateTime.getFullYear() + '-' 
                             + (currentDateTime.getMonth() + 1) + '-'
                             + currentDateTime.getDate() + 'T'
                             + currentDateTime.getHours() + ':'
                             + currentDateTime.getMinutes() + ':'
                             + currentDateTime.getSeconds();

      request({
        url: 'https://api.data.gov.sg/v1/environment/2-hour-weather-forecast',
        headers: {
          'api-key':'ZfiN2gvIs6HAA0T00fiI8Sx1kOMFmC1d',
          'accept' : 'application/json',    
        },
        qs : forecastDateTime,
      }, (error, response, body) => {
        let bodyObj = JSON.parse(body);

        selectedAreaArray = bodyObj.items[0].forecasts.filter((areaObj) => areaObj.area.toUpperCase() == args.town);
        console.log('Area:',     selectedAreaArray[0].area);	
        console.log('Forecast:', selectedAreaArray[0].forecast);
        console.log('Forecast date time:', forecastDateTime);
      });
      break;
    }
  }
}

stateHandler(null);
