# SG Weather 2 Hours Forecast

Fetch 2 hours weather forecast from NEA using Singapore address as input.


## Installation
  1. Run 'npm install'
  2. Register your email with OneMap.sg (https://developers.onemap.sg/signup/)
  3. Put your registered email and password inside the index.js

## Usage

    node index.js -a "<Singapore address>"

For example
    
    node index.js -a "33 sengkang west avenue"
